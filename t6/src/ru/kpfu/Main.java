package ru.kpfu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {

    public static void main(String[] args) {
        try {
            Socket socket = new Socket("127.0.0.1", 8080);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            socket.getInputStream()));
            PrintWriter out = new PrintWriter(
                    socket.getOutputStream());
            out.println("Hello, server");
            out.flush();
            String s = in.readLine();
            System.out.println(s);
            in.close();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void runServer(String[] args) {
	// write your code here
        try {
            ServerSocket serverSocket = new ServerSocket(31337);
            Socket socket = serverSocket.accept();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            socket.getInputStream()));
            PrintWriter out = new PrintWriter(
                    socket.getOutputStream());
            out.println("Hello, type something here (type EXIT to exit): ");
            out.flush();
            boolean exit = false;
            while(!exit) {
                String s = in.readLine();
                out.println(">> " + s);
                out.flush();
                exit = s.equals("EXIT");
            }
            in.close();
            out.close();
            socket.close();
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
