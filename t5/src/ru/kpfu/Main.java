package ru.kpfu;

import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Argon2 argon2 = Argon2Factory.create();

        char[] password = {'p','a','s','s','w','o','r','d'};

        try {
            System.out.println("Password: " + Arrays.toString(password));
            String hash = argon2.hash(10, 65536, 1, password);
            System.out.println("Hash:" + hash);

            System.out.println("Should be OK: " + argon2.verify(hash, password));

            password[1] = 'A';
            System.out.println("Should fail: " + argon2.verify(hash, password));
        } finally {
            argon2.wipeArray(password);
        }
        System.out.println("After wiping: " + Arrays.toString(password));
    }
}
