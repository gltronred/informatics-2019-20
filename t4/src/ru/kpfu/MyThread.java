package ru.kpfu;

import java.util.Random;

public class MyThread extends Thread {
    private int name;

    public MyThread(int name) {
        this.name = name;
    }

    @Override
    public void run() {
        while (true) {
            System.out.println("Hello from thread " + name);
            try {
                int delay = new Random().nextInt(10000);
                Thread.sleep(Math.abs(delay));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
