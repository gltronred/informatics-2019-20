package ru.kpfu;

import java.util.List;

public class SummingThread extends Thread {
    private List<Integer> arr;
    private int start;
    private int count;
    private Integer sum;

    public SummingThread(List<Integer> arr, int start, int count) {
        this.arr = arr;
        this.start = start;
        this.count = count;
        this.sum = 0;
    }

    @Override
    public void run() {
        for (int i = start; i < start + count; i++) {
            sum = sum + arr.get(i);
        }
    }

    public Integer getSum() {
        return sum;
    }
}
