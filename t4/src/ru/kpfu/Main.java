package ru.kpfu;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) throws InterruptedException {
	// write your code here
        //        MyThread t1 = new MyThread(1);
        //        MyThread t2 = new MyThread(2);
        //
        //        t1.start();
        //        t2.start();
//        List<MyThread> ts = new ArrayList<>();
//        for (int i = 0; i<10; i++) {
//            MyThread t = new MyThread(i);
//            t.start();
//            ts.add(t);
//        }
        Random random = new Random();
        List<Integer> arr = new ArrayList<>();
        for (int i = 0; i < 10_000_000; i++) {
            arr.add(random.nextInt());
        }

        long start = System.nanoTime();
        Integer realSum = 0;
        for (Integer x : arr) {
            realSum += x;
        }
        System.out.println(realSum);
        long end = System.nanoTime();
        System.out.println((end - start) + " ns");

        List<SummingThread> ts = new ArrayList<>();
        for (int i=0; i<100; i++) {
            SummingThread t = new SummingThread(arr, i*100_000, 100_000);
            ts.add(t);
        }

        start = System.nanoTime();
        Integer sum = 0;
        for (SummingThread t : ts) {
            t.start();
        }
        for (SummingThread t : ts) {
            t.join();
            sum += t.getSum();
        }
        end = System.nanoTime();
        System.out.println(sum);
        System.out.println((end - start) + " ns");
    }
}
